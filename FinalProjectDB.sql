CREATE TABLE "mf_" (

);

CREATE TABLE "mf_business_profiles" (
  "profile_id" int4 NOT NULL DEFAULT nextval('mf_business_profiles_profile_id_seq'::regclass),
  "company_name" varchar(100) COLLATE "pg_catalog"."default" NOT NULL,
  "uuid" varchar(8) COLLATE "pg_catalog"."default",
  "user_account_id" int4,
  "address" jsonb,
  "phone_number" varchar(20) COLLATE "pg_catalog"."default",
  "email" varchar(50) COLLATE "pg_catalog"."default",
  CONSTRAINT "mf_business_profiles_pk" PRIMARY KEY ("profile_id")
);
ALTER TABLE "mf_business_profiles" OWNER TO "postgres";

CREATE TABLE "mf_contact_types" (

);

CREATE TABLE "mf_contacts" (
  "contact_id" int4 NOT NULL,
  "email" varchar(255),
  "phone_number" int4,
  "website_url" varchar(255),
  "address" varchar(255),
  PRIMARY KEY ("contact_id")
);

CREATE TABLE "mf_cover_letter" (
  "cover_letter_id" int4 NOT NULL,
  "job_position" varchar(255),
  "address" varchar(255),
  "email" varchar(100),
  "work_styles" varchar(255),
  "school_types" varchar(255),
  "years_of_experience" varchar(255),
  "strength_id" int4,
  PRIMARY KEY ("cover_letter_id")
);

CREATE TABLE "mf_cv" (
  "cv_id" int4 NOT NULL,
  "cv_name" varchar(255),
  "contact_id" int4,
  "language_id" int4,
  "skill_id" int4,
  "education_id" int4,
  "work_experience_id" int4,
  "number_of_views" int4,
  PRIMARY KEY ("cv_id")
);

CREATE TABLE "mf_education_types" (
  "education_type_id" int4 NOT NULL,
  "education_type_name" varchar(255),
  PRIMARY KEY ("education_type_id")
);

CREATE TABLE "mf_educations" (
  "education_id" int4 NOT NULL,
  "education_type_id" int4,
  "certificate" varchar(255),
  "start_date" date,
  "end_date" date,
  PRIMARY KEY ("education_id")
);

CREATE TABLE "mf_languages" (
  "language_id" int4 NOT NULL,
  "language_name" varchar(255),
  "language_level" varchar(255),
  PRIMARY KEY ("language_id")
);

CREATE TABLE "mf_reports" (
  "report_id" int4 NOT NULL,
  "report_name" varchar(255),
  "profile_id" int4,
  PRIMARY KEY ("report_id")
);

CREATE TABLE "mf_roles" (
  "role_id" int4 NOT NULL DEFAULT nextval('mf_roles_role_id_seq'::regclass),
  "role_name" varchar(10) COLLATE "pg_catalog"."default" NOT NULL,
  CONSTRAINT "mf_roles_pk" PRIMARY KEY ("role_id")
);
ALTER TABLE "mf_roles" OWNER TO "postgres";

CREATE TABLE "mf_skills" (
  "skill_id" int4 NOT NULL,
  "skill_name" varchar(255),
  "skill_level" varchar(255),
  PRIMARY KEY ("skill_id")
);

CREATE TABLE "mf_strengths" (
  "strength_id" int4 NOT NULL,
  "keyword" varchar(255),
  "description" varchar(255),
  PRIMARY KEY ("strength_id")
);

CREATE TABLE "mf_user_accounts" (
  "user_id" int4 NOT NULL DEFAULT nextval('mf_user_accounts_user_id_seq'::regclass),
  "username" varchar(100) COLLATE "pg_catalog"."default" NOT NULL,
  "password" varchar(100) COLLATE "pg_catalog"."default" NOT NULL,
  "role_id" int4,
  CONSTRAINT "mf_user_accounts_pk" PRIMARY KEY ("user_id")
);
ALTER TABLE "mf_user_accounts" OWNER TO "postgres";
CREATE UNIQUE INDEX "mf_user_accounts_username_uindex" ON "mf_user_accounts" USING btree (
  "username" COLLATE "pg_catalog"."default" "pg_catalog"."text_ops" ASC NULLS LAST
);

CREATE TABLE "mf_user_profiles" (
  "profile_id" int4 NOT NULL DEFAULT nextval('mf_user_profiles_profile_id_seq'::regclass),
  "first_name" varchar(100) COLLATE "pg_catalog"."default" NOT NULL,
  "last_name" varchar(100) COLLATE "pg_catalog"."default" NOT NULL,
  "uuid" varchar(8) COLLATE "pg_catalog"."default",
  "address" jsonb,
  "date_of_birth" date NOT NULL,
  "email" varchar(50) COLLATE "pg_catalog"."default",
  "phone_number" varchar(20) COLLATE "pg_catalog"."default",
  "user_account_id" int4,
  "education_id" int4,
  "work_id" int4,
  "cv_id" int4,
  "cover_letter_id" int4,
  CONSTRAINT "mf_user_profiles_pk" PRIMARY KEY ("profile_id")
);
ALTER TABLE "mf_user_profiles" OWNER TO "postgres";

CREATE TABLE "mf_work_experiences" (
  "work_experience_id" int4 NOT NULL,
  "company_name" varchar(255) NOT NULL,
  "company_position" varchar(255) NOT NULL,
  "start_date" date NOT NULL,
  "end_date" date NOT NULL,
  PRIMARY KEY ("work_experience_id")
);

ALTER TABLE "mf_business_profiles" ADD CONSTRAINT "mf_business_profiles_mf_user_accounts_user_id_fk" FOREIGN KEY ("user_account_id") REFERENCES "mf_user_accounts" ("user_id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "mf_contacts" ADD CONSTRAINT "fk_mf_contacts_mf_cv_1" FOREIGN KEY ("contact_id") REFERENCES "mf_cv" ("contact_id");
ALTER TABLE "mf_cover_letter" ADD CONSTRAINT "fk_mf_cv_mf_user_profiles_1" FOREIGN KEY ("cv_id") REFERENCES "mf_user_profiles" ("education_id");
ALTER TABLE "mf_education_types" ADD CONSTRAINT "fk_mf_education_types_mf_educations_1" FOREIGN KEY ("education_type_id") REFERENCES "mf_educations" ("education_id");
ALTER TABLE "mf_educations" ADD CONSTRAINT "fk_mf_educations_mf_user_profiles_1" FOREIGN KEY ("education_id") REFERENCES "mf_user_profiles" ("user_account_id");
ALTER TABLE "mf_educations" ADD CONSTRAINT "fk_mf_educations_mf_cv_1" FOREIGN KEY ("education_id") REFERENCES "mf_cv" ("education_id");
ALTER TABLE "mf_languages" ADD CONSTRAINT "fk_mf_languages_mf_cv_1" FOREIGN KEY ("language_id") REFERENCES "mf_cv" ("language_id");
ALTER TABLE "mf_reports" ADD CONSTRAINT "fk_mf_reports_mf_user_profiles_1" FOREIGN KEY ("profile_id") REFERENCES "mf_user_profiles" ("profile_id");
ALTER TABLE "mf_skills" ADD CONSTRAINT "fk_mf_skills_mf_cv_1" FOREIGN KEY ("skill_id") REFERENCES "mf_cv" ("skill_id");
ALTER TABLE "mf_strengths" ADD CONSTRAINT "fk_mf_strengths_mf_cover_letter_1" FOREIGN KEY ("strength_id") REFERENCES "mf_cover_letter" ("strength_id");
ALTER TABLE "mf_user_accounts" ADD CONSTRAINT "mf_user_accounts_mf_roles_role_id_fk" FOREIGN KEY ("role_id") REFERENCES "mf_roles" ("role_id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "mf_user_profiles" ADD CONSTRAINT "mf_user_profiles_mf_user_accounts_user_id_fk" FOREIGN KEY ("user_account_id") REFERENCES "mf_user_accounts" ("user_id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "mf_user_profiles" ADD CONSTRAINT "fk_mf_user_profiles_mf_cv_1" FOREIGN KEY ("cv_id") REFERENCES "mf_cv" ("cv_id");
ALTER TABLE "mf_work_experiences" ADD CONSTRAINT "fk_mf_work_experiences_mf_user_profiles_1" FOREIGN KEY ("staff_id") REFERENCES "mf_user_profiles" ("user_account_id");
ALTER TABLE "mf_work_experiences" ADD CONSTRAINT "fk_mf_work_experiences_mf_cv_1" FOREIGN KEY ("work_experience_id") REFERENCES "mf_cv" ("work_experience_id");

